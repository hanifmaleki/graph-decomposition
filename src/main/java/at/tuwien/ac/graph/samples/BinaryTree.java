package at.tuwien.ac.graph.samples;

import at.tuwien.ac.graph.graph.Graph;
import at.tuwien.ac.graph.graph.UndirectedGraph;
import at.tuwien.ac.graph.graph.Vertex;

/**
 * Created by e1528895 on 12/23/17.
 */
public class BinaryTree {

    private final int height;

    public BinaryTree(int height){
        this.height = height;
    }
    public Graph getGraph(){

        Graph graph = new UndirectedGraph();

        int vertexCount= (int) (Math.pow(2, height)-1);

        for(int i=0; i < vertexCount; i++){
            //Vertex vertex = new Vertex(String.valueOf(vertexCount), vertexCount);
            graph.addVertex(String.valueOf(i));
        }

        for(int i = 0; i < vertexCount/2; i++){
            Vertex parent = graph.getVertexByName(String.valueOf(i));
            Vertex child1 = graph.getVertexByName(String.valueOf(2*i+1));
            Vertex child2 = graph.getVertexByName(String.valueOf(2*i+2));
            graph.addEdge(parent, child1);
            graph.addEdge(parent, child2);
        }

        return graph;

    }

    public static void main(String[] args) {
        Graph graph = new BinaryTree(4).getGraph();
        System.out.println(graph);
    }
}
