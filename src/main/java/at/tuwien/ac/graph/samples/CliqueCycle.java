package at.tuwien.ac.graph.samples;

import at.tuwien.ac.graph.graph.UndirectedGraph;
import at.tuwien.ac.graph.graph.Graph;
import at.tuwien.ac.graph.graph.Vertex;

/**
 * Created by hanif on 3/29/17.
 */
public class CliqueCycle {
    int numberOfCliques = 3;
    int cliqueSize = 6;
    int connectorCount = 2;

    public CliqueCycle(int numberOfCliques, int connectorCount) {
        this.numberOfCliques = numberOfCliques;
        this.connectorCount = connectorCount;
        cliqueSize = numberOfCliques * connectorCount ;
    }

    public Graph getGraph() {
        Graph graph = new UndirectedGraph();
        for (int i = 1; i <= numberOfCliques; i++) {

            for (int j = 1; j <= cliqueSize; j++) {
                Vertex vertex = graph.addVertex("v" + i + "," + j);
                for (int k = 1; k < j; k++) {
                    Vertex to = graph.getVertexByName("v" + i + "," + k);
                    graph.addEdge(vertex, to);
                }
            }

            for (int j = 1; j <= connectorCount; j++) {
                //if((i==3)&&(j==1))
                //    continue;
                Vertex vertex = graph.addVertex("c" + i + "," + j);
                Vertex to = graph.getVertexByName("v" + i + "," + j);
                graph.addEdge(vertex, to);

                if (i != 1) {
                    //if((i==4)&&(j==1))
                    //    continue;
                    vertex = graph.getVertexByName("v" + i + "," + (cliqueSize - j + 1));
                    to = graph.getVertexByName("c" + (i - 1) + "," + j);
                    graph.addEdge(vertex, to);
                }
            }
        }
        for (int j = 1; j <= connectorCount; j++) {
            Vertex vertex = graph.getVertexByName("v1," + (cliqueSize - j + 1));
            Vertex to = graph.getVertexByName("c" + (numberOfCliques ) + "," + j);
            graph.addEdge(vertex, to);
        }
        return graph;
    }
}
