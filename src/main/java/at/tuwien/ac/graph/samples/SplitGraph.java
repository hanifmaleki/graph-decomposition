package at.tuwien.ac.graph.samples;
  
import at.tuwien.ac.graph.graph.UndirectedGraph;
import at.tuwien.ac.graph.graph.Graph;
import at.tuwien.ac.graph.graph.Vertex;
import at.tuwien.ac.graph.newgraph.GraphNew;
import at.tuwien.ac.graph.utils.IncidentGraphHelper;


public class SplitGraph {

    int size = 20;

    public SplitGraph(int size) {
        this.size = size;
    }

    public Graph getGraph() {
        Graph graph = new UndirectedGraph();
        for (int i = 1; i <= size; i++) {
            Vertex vertex = graph.addVertex("c" + i);
            for (int j = 1; j <= size; j++) {
                Vertex to = graph.addVertex("x" + i + "-" + j);
                graph.addEdge(vertex, to);
            }
            for (int j = 1; j < i; j++) {
                Vertex to = graph.getVertexByName("c" + j);
                graph.addEdge(vertex, to);
            }
        }
        return graph;
    }

    public static void main(String[] args) {
        Graph graph = new SplitGraph(20).getGraph();
        GraphNew newStructure = IncidentGraphHelper.getNewStructure(graph);
        System.out.println(newStructure);
    }
}

