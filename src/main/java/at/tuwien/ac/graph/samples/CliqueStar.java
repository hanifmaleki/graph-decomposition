package at.tuwien.ac.graph.samples;

import at.tuwien.ac.graph.graph.Graph;
import at.tuwien.ac.graph.graph.UndirectedGraph;
import at.tuwien.ac.graph.graph.Vertex;
import at.tuwien.ac.graph.newgraph.GraphNew;
import at.tuwien.ac.graph.newgraph.GraphNewHelper;
import at.tuwien.ac.graph.ops.executors.SimpleSatExecutor;
import at.tuwien.ac.graph.ops.expriments.SimpleSatExperiment;
import at.tuwien.ac.graph.ops.expriments.VertexEntity;
import at.tuwien.ac.graph.ops.instance.Instance;
import at.tuwien.ac.graph.sat.NewSatProducer;
import at.tuwien.ac.graph.utils.IncidentGraphHelper;
import at.tuwien.ac.graph.utils.MPSReaderHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e1528895 on 9/15/17.
 * It is a c-clque in the center and n (c+1)-clique connecting to it
 */
public class CliqueStar {

    int c ;
    int n ;

    public CliqueStar(int c, int n){
        this.c = c ;
        this.n = n ;
    }

    public Graph getGraph(){
        Graph graph = new UndirectedGraph();

        for(int i= 0; i <= n; i++) {
            if(i==0)
                createClique(graph, String.valueOf(i), c);
            else
                createClique(graph, String.valueOf(i), c+1);
        }

        for(int i = 1; i <= n; i++){
            for(int j = 1; j <=c; j++){
                Vertex vertex1 = graph.getVertexByName("v0," + j);
                Vertex vertex2 = graph.getVertexByName("v" + i + "," + j);
                graph.addEdge(vertex1, vertex2);
            }
        }


        return graph;
    }

    public void createClique(Graph graph, String cliqueNumber, int c) {
        for (int j = 1; j <= c; j++) {
            Vertex vertex = graph.addVertex("v"+cliqueNumber+"," + j);
        }

        for (int i = 1; i <= c; i++)
            for (int j = i + 1; j <= c; j++) {
                Vertex from = graph.getVertexByName("v"+cliqueNumber+"," + i);
                Vertex to = graph.getVertexByName("v"+cliqueNumber+"," + j);
                graph.addEdge(from, to);
            }
    }

    public static void main(String[] args) {
        Graph oldGraph = new CliqueStar(5, 3).getGraph();
        GraphNew graph = IncidentGraphHelper.getNewStructure(oldGraph);
        //Instance instance = new Instance();
        //IMana
        System.out.println("Graph Size "+ graph.getSize());
        System.out.println("Graph.Edges "+ graph.getEdgeSize());
        System.out.println("Graph.MaxDegree "+ graph.getMaxDegree());
        System.out.println("Graph.Max Degree Count "+ graph.getMaxDegreeList().size());
        SimpleSatExperiment experiment = new SimpleSatExecutor().doSimpleExperiment(graph, 6, 5, 3000);
        System.out.println(experiment);
        List<VertexEntity> selectedVertices = experiment.getSelectedVertices();
        List<Integer> fvd = new ArrayList<>();
        for(VertexEntity entity: selectedVertices){
            Integer vertex = entity.getVertex();
            fvd.add(vertex);
        }
        GraphNewHelper.testAnsShowBackdoor(graph, fvd);
    }
}
