package at.tuwien.ac.graph.samples;

import at.tuwien.ac.graph.graph.Graph;
import at.tuwien.ac.graph.graph.UndirectedGraph;
import at.tuwien.ac.graph.graph.Vertex;

/**
 * Created by root on 5/2/17.
 */
public class Grid {

    private final int gridSize;

    public Grid(int gridSize){
        this.gridSize = gridSize;
    }

    public Graph getGraph(){
        Graph graph = new UndirectedGraph();
        for(int i = 0; i < gridSize; i++)
            for(int j = 0; j < gridSize; j++)
                graph.addVertex("n"+i+","+j);

        for(int i = 1; i < gridSize; i++)
            for(int j = 0; j < gridSize; j++) {
                Vertex v1 = graph.getVertexByName("n"+i+","+j);
                Vertex v2 = graph.getVertexByName("n"+(i-1)+","+j);
                graph.addEdge(v1, v2);
            }

        for(int i = 0; i < gridSize; i++)
            for(int j = 1; j < gridSize; j++) {
                Vertex v1 = graph.getVertexByName("n"+i+","+j);
                Vertex v2 = graph.getVertexByName("n"+i+","+(j-1));
                graph.addEdge(v1, v2);
            }

        return graph;
    }
}
