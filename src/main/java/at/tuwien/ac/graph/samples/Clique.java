package at.tuwien.ac.graph.samples;

import at.tuwien.ac.graph.graph.Graph;
import at.tuwien.ac.graph.graph.UndirectedGraph;
import at.tuwien.ac.graph.graph.Vertex;

/**
 * Created by hanif on 3/29/17.
 */
public class Clique {

    int cliqueSize = 10;

    public Clique(int n){
        this.cliqueSize = n ;
    }

    public Graph getGraph() {
        Graph graph = new UndirectedGraph();


            for (int j = 1; j <= cliqueSize; j++) {
                Vertex vertex = graph.addVertex("v" + j);

            }

            for (int i = 1; i <= cliqueSize; i++)
                for (int j = i+1; j <= cliqueSize; j++) {
                    Vertex from = graph.getVertexByName("v" + i);
                    Vertex to = graph.getVertexByName("v" + j);
                    graph.addEdge(from, to);
                }


        return graph;
    }
}
