package at.tuwien.ac.graph.samples;

import at.tuwien.ac.graph.graph.Graph;
import at.tuwien.ac.graph.graph.UndirectedGraph;
import at.tuwien.ac.graph.graph.Vertex;

/**
 * Created by root on 4/21/17.
 */
public class SimpleTriangleGraph {

    public Graph getGraph(){
        Graph graph = new UndirectedGraph();
        Vertex c1 = graph.addVertex("c1");
        Vertex c2 = graph.addVertex("c2");
        Vertex c3 = graph.addVertex("c3");
        Vertex c4 = graph.addVertex("c4");

        graph.addEdge(c1,c2);
        graph.addEdge(c2, c3);
        graph.addEdge(c3, c1);
        graph.addEdge(c2, c4);
        graph.addEdge(c3, c4);
        return graph;
    }
}
